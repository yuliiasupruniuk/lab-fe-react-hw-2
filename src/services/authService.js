const URL = 'http://localhost:4000';

async function register(userInfo) {
	const response = await fetch(`${URL}/register`, {
		method: 'POST',
		body: JSON.stringify(userInfo),
		headers: {
			'Content-Type': 'application/json',
		},
	}).then(function (response) {
		return response.json();
	});

	return response;
}

async function login(userInfo) {
	const response = await fetch(`${URL}/login`, {
		method: 'POST',
		body: JSON.stringify(userInfo),
		headers: {
			'Content-Type': 'application/json',
		},
	}).then(function (response) {
		return response.json();
	});

	setToken(response);
	return response;
}

function logout() {
	localStorage.removeItem('token');
	localStorage.removeItem('user');
}

function setToken(response) {
	if (response.result && response.successful) {
		const user = response.user;
		localStorage.setItem('token', response.result);
		localStorage.setItem('user', JSON.stringify(user));
	} else {
		localStorage.clear();
	}
}

function getToken() {
	const token = localStorage.getItem('token');
	return token;
}

function isAuthenticated() {
	if (getToken()) {
		return true;
	}
	return false;
}

function getUserInfo() {
	const userInfo = localStorage.getItem('user');
	const user = JSON.parse(userInfo);
	return user;
}

export { register, login, isAuthenticated, logout, getUserInfo };
