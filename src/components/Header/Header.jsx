import Button from '../common/Button/Button';
import Logo from './components/Logo/Logo';
import { getUserInfo, logout } from '../../services/authService';
import { useNavigate } from 'react-router-dom';

function Header() {
	const navigate = useNavigate();

	const logoutUser = () => {
		logout();
		navigate('/login');
	};

	const user = getUserInfo();

	return (
		<nav className='navbar navbar-light alert-primary text-dark'>
			<a className='navbar-brand' href='/'>
				<Logo />
			</a>

			<div className='d-flex align-items-end'>
				<h6 className='mr-3'>{user.name}</h6>
				<Button buttonText='Logout' color='warning' onClick={logoutUser} />
			</div>
		</nav>
	);
}
export default Header;
